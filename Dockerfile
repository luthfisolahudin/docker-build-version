FROM php:apache

ARG LAST_COMMIT_INFO

ENV TZ="Asia/Jakarta"

RUN if [ -n "$LAST_COMMIT_INFO" ]; then \
		echo "Last-Commit: $LAST_COMMIT_INFO" >> /var/www/html/version.txt; \
	fi; \
	echo "Last-Build: $(date -R)" >> /var/www/html/version.txt
